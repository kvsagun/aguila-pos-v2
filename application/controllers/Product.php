<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	/****************************************/
	/****************************************/
	/*			 DEVELOPED BY:	 			*/
	/*			 KENN JEUS SAGUN 			*/
	/****************************************/
	/****************************************/

	function __construct()
    {
        parent::__construct(); 
        $this->load->model('Product_model');
        $this->load->helper('array_helper');
        $this->load->helper('excel_helper');
    }
    
	public function is_logged_in() {
		$access = $this->session->userdata('pos_user_info');
		
		if(!array_check($access)) {
			header("Location: ".base_url()."login", true, 301);			
		}
	}

	public function index()
	{
        $this->load->model('Category_model');

		$this->is_logged_in();
		$data['access']				= $this->session->userdata('pos_user_info');
        $data['user_info'] 			= $this->User_model->get_users($data['access']['id']);

       	$data['user_permissions']	= json_decode($data['user_info'][0]['permissions']);

       	if ($data['user_permissions']->product[0] == "0")
       		show_404();

		$data['nav'] 				= 'Product';
		$data['css']				= [''];
		$data['javascripts']		= ['modules/product.js?v1.0'];
		$data['categories'] 		= $this->Category_model->get_categories();

		$this->load->view('includes/header', $data);
		$this->load->view('product/index', $data);
		$this->load->view('includes/footer', $data);
	}

    public function get_product() 
    {
    	if($this->input->post()){
	        $result['data'] = $this->Product_model->get_product();

	        header('Content-Type: application/json');
	        echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_product_by_id() 
    {
    	if($this->input->post()){
	        $result['data'] = $this->Product_model->get_product($id = $this->input->post('id'));

	        header('Content-Type: application/json');
	        echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_pagination() 
    {
    	if($this->input->post()){
	        $result['data'] = $this->Product_model->get_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

	        header('Content-Type: application/json');
        echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_table_product() 
    {
    	if($this->input->post()){
	        $result['data'] = $this->Product_model->get_product($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

	        header('Content-Type: application/json');
	        echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function save_product()
    {
    	if($this->input->post()){
	    	if($this->input->post('action_type') == "delete"){
				$result['data'] = $this->Product_model->delete_product($this->input->post());
	    	}else{

	    		$product_image 			 = "";
		    	$config['upload_path']   = FCPATH."/".LOCAL_UPLOAD_PATH;
		        $config['allowed_types'] = 'gif|jpg|jpeg|png';
		        $config['overwrite'] 	 = false;
		        $config['remove_spaces'] = true;
		        $config['max_width']     = 640;
		        $config['max_height']    = 480;

		        $this->load->library('upload',$config);
		        if($this->upload->do_upload('product_image')){
		        	$data 				= array('upload_data' => $this->upload->data());
		            $product_image 		= $data['upload_data']['file_name'];
		    	}else{
		    		$result['error'] 	= $this->upload->display_errors();
		    	}

		    	if($this->input->post('action_type') == "create"){
					$result['data'] 	= $this->Product_model->add_product($this->input->post(), $product_image);
		    	}else{
					$result['data'] 	= $this->Product_model->update_product($this->input->post(), $product_image);
		    	}

	    	}
			
			header("Content-Type: application/json", true);
			$this->output->set_output(print(json_encode($result)));
			exit();
        }else{
            show_404();
        }
	}
}
