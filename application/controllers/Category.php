<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	/****************************************/
	/****************************************/
	/*			 DEVELOPED BY:	 			*/
	/*			 KENN JEUS SAGUN 			*/
	/****************************************/
	/****************************************/

	function __construct()
    {
        parent::__construct(); 
        $this->load->model('Category_model');
        $this->load->helper('array_helper');
        $this->load->helper('excel_helper');
    }
    
	public function is_logged_in() {
		$access = $this->session->userdata('pos_user_info');
		
		if(!array_check($access)) {
			header("Location: ".base_url()."login", true, 301);			
		}
	}

	public function index()
	{
		$this->is_logged_in();
		$data['access']				= $this->session->userdata('pos_user_info');
        $data['user_info'] 			= $this->User_model->get_users($data['access']['id']);

       	$data['user_permissions']	= json_decode($data['user_info'][0]['permissions']);

       	if ($data['user_permissions']->category[0] == "0")
       		show_404();
       	
		$data['nav'] 				= 'Category';
		$data['css']				= [''];
		$data['javascripts']		= ['modules/category.js?v1.0'];

		$this->load->view('includes/header', $data);
		$this->load->view('category/index', $data);
		$this->load->view('includes/footer', $data);
	}

    public function get_category() 
    {
        if($this->input->post()){
            $result['data'] = $this->Category_model->get_categories();

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_category_by_id() 
    {
        if($this->input->post()){
            $result['data'] = $this->Category_model->get_categories($id = $this->input->post('id'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function get_pagination() 
    {
        if($this->input->post()){
            $result['data'] = $this->Category_model->get_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function load_table_category() 
    {
        if($this->input->post()){
            $result['data'] = $this->Category_model->get_categories($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

            header('Content-Type: application/json');
            echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function save_categories()
    {
        if($this->input->post()){
        	if($this->input->post('action_type') == "create"){
    			$result['data'] = $this->Category_model->add_categories($this->input->post());
        	}else if($this->input->post('action_type') == "update"){
    			$result['data'] = $this->Category_model->update_categories($this->input->post());
        	}else{
    			$result['data'] = $this->Category_model->delete_categories($this->input->post());
        	}
    		
    		header("Content-Type: application/json", true);
    		$this->output->set_output(print(json_encode($result)));
    		exit();
        }else{
            show_404();
        }
	}
}
