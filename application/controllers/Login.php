<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/****************************************/
	/****************************************/
	/*			 DEVELOPED BY:	 			*/
	/*			 KENN JEUS SAGUN 			*/
	/****************************************/
	/****************************************/

	function __construct()
    {
        parent::__construct(); 
        $this->load->model('Login_model');
        $this->load->helper('array_helper');
    }
    
	public function is_logged_in() {
		$access = $this->session->userdata('pos_user_info');
		
		if(array_check($access)) {

			$data['user_info'] 			= $this->User_model->get_users($data['access']['id']);
	       	$data['user_permissions']	= json_decode($data['user_info'][0]['permissions']);

	       	if ($data['user_permissions']->dashboard[0] != "0"){
				header("Location: ".base_url()."dashboard", true, 301);	
	       	}else if ($data['user_permissions']->pos[0] != "0"){
				header("Location: ".base_url()."pos", true, 301);	
	       	}else if ($data['user_permissions']->sales[0] != "0"){
				header("Location: ".base_url()."sales", true, 301);	
	       	}else if ($data['user_permissions']->pending[0] != "0"){
				header("Location: ".base_url()."pending", true, 301);	
	       	}else if ($data['user_permissions']->inventory[0] != "0"){
				header("Location: ".base_url()."inventory", true, 301);	
	       	}else{
				header("Location: ".base_url()."profile", true, 301);	
	       	}		
		}
	}

	public function index()
	{
		$this->is_logged_in();
		$data['nav'] 				= 'Login';
		$data['css']				= [''];
		$data['javascripts']		= [''];

		$this->load->view('login', $data);
	}

	public function authorize_user()
	{
		// $this->is_logged_in();
		
		$result = $this->Login_model->authorize_user($this->input->post());

		if(array_check($result)) {

			if($result['is_deleted'] 		== 0) {

				unset($result['password']);
				$this->session->set_userdata('pos_user_info', $result);
				$this->Login_model->update_last_login($result['id']);		
				
				$response['status'] 		= true;

        		$data['user_info'] 			= $this->User_model->get_users($result['id']);
		       	$data['user_permissions']	= json_decode($data['user_info'][0]['permissions']);

		       	if ($data['user_permissions']->dashboard[0] != "0"){
					$response['message'] 	= base_url() . "dashboard";	
		       	}else if ($data['user_permissions']->pos[0] != "0"){
					$response['message'] 	= base_url() . "pos";	
		       	}else if ($data['user_permissions']->sales[0] != "0"){
					$response['message'] 	= base_url() . "sales";	
		       	}else if ($data['user_permissions']->pending[0] != "0"){
					$response['message'] 	= base_url() . "pending";	
		       	}else if ($data['user_permissions']->inventory[0] != "0"){
					$response['message'] 	= base_url() . "inventory";	
		       	}else{
					$response['message'] 	= base_url() . "profile";	
		       	}

				echo json_encode($response);

			} else {

				$response['status'] 		= false;
				$response['message'] 		= "Please contact the administrator for your account";
				echo json_encode($response);

			}
			
		} else {

			$response['status'] = false;
			$response['message'] = "Incorrect Username or Password";
			echo json_encode($response);

		}
	}

	public function logout()
	{
		$this->session->unset_userdata('pos_user_info');
		header("Location: ".base_url()."login", true, 301);
	}

	public function reset_forgotten_password($token = ""){
		if($token != ""){

			$data['info'] 			= $this->Login_model->check_forgot_password_token($token);

			$data['nav'] 			= 'Reset Password';
			$data['css']			= [''];
			$data['javascripts']	= [''];

			$this->load->view('reset_password', $data);

		}else{
			show_404();
		}

	}

	public function send_forgot_password_email()
	{
		if($this->input->post()){
			$this->load->library('email');

			$result 						= $this->Login_model->check_forgot_password_email($this->input->post());


			if(array_check($result)) {

				if($result['is_deleted'] 	== 0) {

					$input['token']			= uniqid();
					$input['user_id']		= $result['id'];

					$data['first_name'] 	= $result['first_name'];
					$data['message'] 		= 'Seems like you forgot your password for KVSAGUN POS. If this is true, click below to reset your password.<br><br><center><a href="' . base_url() . 'login/reset_forgotten_password/' . $input['token'] . '" style="color: #fff; background-color: #337ab7; border-color: #2e6da4;display: block; margin-bottom: 0; font-weight: 400; text-align: center; white-space: nowrap; vertical-align: middle; -ms-touch-action: manipulation; touch-action: manipulation; cursor: pointer; background-image: none; border: 1px solid transparent; padding: 6px 12px; font-size: 14px; line-height: 1.42857143; border-radius: 4px; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; text-decoration: none;">Reset My Password</a></center><br>This link will be valid for the next <b>30 minutes</b>.<br><br>If you did not forgot your password you can ignore this email.';

					$save_code 				= $this->Login_model->save_forgot_password_token($input);

					$mail_subject  			= 'KVSAGUN POS - Forgot Password';
					$mail_message 			= $this->load->view('email_template/index', $data, true);
			
					$this->email->from($this->config->item('email_address'),$this->config->item('email_sender_name'));
			        $this->email->to($result['email']);
			        $this->email->subject($mail_subject);
			        $this->email->message($mail_message);


					$response['status'] 	= true;
					// $response['message']= "The forgot password instruction was sent to your email!";
					// echo json_encode($response);
			
				    if($this->email->send())
			        {
						$response['message']= "The forgot password instruction was sent to your email!";
						echo json_encode($response);
			        }
			        else
			        {
			        	$response['message']= $this->email->print_debugger();
						echo json_encode($response);
			        };

				} else {

					$response['status'] 	= false;
					$response['message'] 	= "Please contact the administrator for your account";
					echo json_encode($response);

				}

			} else {

				$response['status'] 		= false;
				$response['message'] 		= "User does not exist!";
				echo json_encode($response);

			}

	    }else{
	    	show_404();
	    }
	}
}
