<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	/****************************************/
	/****************************************/
	/*			 DEVELOPED BY:	 			*/
	/*			 KENN JEUS SAGUN 			*/
	/****************************************/
	/****************************************/

	function __construct()
    {
        parent::__construct(); 
        $this->load->helper('array_helper');
        $this->load->helper('excel_helper');
    }
    
	public function is_logged_in() {
		$access = $this->session->userdata('pos_user_info');
		
		if(!array_check($access)) {
			header("Location: ".base_url()."login", true, 301);			
		}
	}

	public function index()
	{
		$this->is_logged_in();
		$data['access']				= $this->session->userdata('pos_user_info');
        $data['user_info'] 			= $this->User_model->get_users($data['access']['id']);

       	$data['user_permissions']	= json_decode($data['user_info'][0]['permissions']);

		$data['nav'] 				= 'Dashboard';
		$data['css']				= [''];
		$data['javascripts']		= ['modules/profile.js'];

		$this->load->view('includes/header', $data);
		$this->load->view('profile/index', $data);
		$this->load->view('includes/footer', $data);
	}
}
