<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Controller {

	/****************************************/
	/****************************************/
	/*			 DEVELOPED BY:	 			*/
	/*			 KENN JEUS SAGUN 			*/
	/****************************************/
	/****************************************/

	function __construct()
    {
        parent::__construct(); 
        $this->load->model('General_model');
        $this->load->helper('array_helper');
        $this->load->helper('excel_helper');
    }
    
	public function is_logged_in() {
		$access = $this->session->userdata('pos_user_info');
		
		if(!array_check($access)) {
			header("Location: ".base_url()."login", true, 301);			
		}
	}

	public function index()
	{
		$this->is_logged_in();
		$data['access']				= $this->session->userdata('pos_user_info');
        $data['user_info'] 			= $this->User_model->get_users($data['access']['id']);

       	$data['user_permissions']	= json_decode($data['user_info'][0]['permissions']);

       	if ($data['user_permissions']->system_setup[0] == "0")
       		show_404();

		$data['nav'] 				= 'General';
		$data['css']				= [''];
		$data['javascripts']		= ['modules/general.js'];

		$this->load->view('includes/header', $data);
		$this->load->view('general/index', $data);
		$this->load->view('includes/footer', $data);
	}

    public function get_company() 
    {
        if($this->input->post()){
	        $result['data'] = $this->General_model->get_company();

	        header('Content-Type: application/json');
	        echo json_encode($result);
	    }else{
            show_404();
        }
    }

    public function update_company()
    {
    	if($this->input->post()){
			$logo 			 		 = "";
	    	$config['upload_path']   = FCPATH."/assets/img/";
	        $config['allowed_types'] = 'gif|jpg|jpeg|png';
	        $config['overwrite'] 	 = false;
	        $config['remove_spaces'] = true;
	        $config['max_width']     = 640;
	        $config['max_height']    = 480;

	        $this->load->library('upload',$config);
	        if($this->upload->do_upload('logo')){
	        	$data 				= array('upload_data' => $this->upload->data());
	            $logo 				= $data['upload_data']['file_name'];
	    	}else{
	    		$result['error'] 	= $this->upload->display_errors();
	    	}

	        $result['data'] = $this->General_model->update_company($id = $this->input->post(), $logo);

	        header('Content-Type: application/json');
	        echo json_encode($result);
	    }else{
            show_404();
        }
    }

    public function get_company_by_id() 
    {
        if($this->input->post()){
	        $result['data'] = $this->General_model->get_company($id = $this->input->post('id'));

	        header('Content-Type: application/json');
	        echo json_encode($result);
	    }else{
            show_404();
        }
    }

    public function get_senior() 
    {
        if($this->input->post()){
	        $result['data'] = $this->General_model->get_senior();

	        header('Content-Type: application/json');
	        echo json_encode($result);
	    }else{
            show_404();
        }
    }

    public function get_senior_by_id() 
    {
        if($this->input->post()){
	        $result['data'] = $this->General_model->get_senior($id = $this->input->post('id'));

	        header('Content-Type: application/json');
	        echo json_encode($result);
	    }else{
            show_404();
        }
    }

    public function update_senior() 
    {
        if($this->input->post()){
	        $result['data'] = $this->General_model->update_senior($id = $this->input->post());

	        header('Content-Type: application/json');
	        echo json_encode($result);
	    }else{
            show_404();
        }
    }

    public function get_vat() 
    {
        if($this->input->post()){
	        $result['data'] = $this->General_model->get_vat();

	        header('Content-Type: application/json');
	        echo json_encode($result);
	    }else{
            show_404();
        }
    }

    public function get_vat_by_id() 
    {
        if($this->input->post()){
	        $result['data'] = $this->General_model->get_vat($id = $this->input->post('id'));

	        header('Content-Type: application/json');
	        echo json_encode($result);
	    }else{
            show_404();
        }
    }

    public function update_vat() 
    {
        if($this->input->post()){
	        $result['data'] = $this->General_model->update_vat($id = $this->input->post());

	        header('Content-Type: application/json');
	        echo json_encode($result);
	    }else{
            show_404();
        }
    }
}
