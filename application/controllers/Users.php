<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	/****************************************/
	/****************************************/
	/*			 DEVELOPED BY:	 			*/
	/*			 KENN JEUS SAGUN 			*/
	/****************************************/
	/****************************************/

	function __construct()
    {
        parent::__construct(); 
        $this->load->helper('array_helper');
        $this->load->helper('excel_helper');
    }
    
	public function is_logged_in() {
		$access = $this->session->userdata('pos_user_info');
		
		if(!array_check($access)) {
			header("Location: ".base_url()."login", true, 301);			
		}
	}

	public function index()
	{
		$this->is_logged_in();
		$data['access']				= $this->session->userdata('pos_user_info');
        $data['user_info'] 			= $this->User_model->get_users($data['access']['id']);

       	$data['user_permissions']	= json_decode($data['user_info'][0]['permissions']);

       	if ($data['user_permissions']->users[0] == "0")
       		show_404();

		$data['nav'] 				= 'Users';
		$data['css']				= [''];
		$data['javascripts']		= ['modules/users.js?v1.0'];

		$this->load->view('includes/header', $data);
		$this->load->view('users/index', $data);
		$this->load->view('includes/footer', $data);
	}

    public function get_users() 
    {
        if($this->input->post()){
	        $result['data'] = $this->User_model->get_users();

	        header('Content-Type: application/json');
	        echo json_encode($result);
	    }else{
            show_404();
        }
    }

    public function get_users_by_id() 
    {
        if($this->input->post()){
	        $result['data'] = $this->User_model->get_users($id = $this->input->post('id'));

	        header('Content-Type: application/json');
	        echo json_encode($result);
	    }else{
            show_404();
        }
    }

    public function get_pagination() 
    {
        if($this->input->post()){
	        $result['data'] = $this->User_model->get_pagination($limit = $this->input->post('limit'), $search = $this->input->post('search'));

	        header('Content-Type: application/json');
	        echo json_encode($result);
    	}else{
            show_404();
        }
    }

    public function load_table_users() 
    {
    	if($this->input->post()){
	        $result['data'] = $this->User_model->get_users($id = 0, $limit = $this->input->post('limit'), $offset = $this->input->post('offset'), $search = $this->input->post('search'));

	        header('Content-Type: application/json');
	        echo json_encode($result);
        }else{
            show_404();
        }
    }

    public function save_users()
    {
    	if($this->input->post()){
	    	if($this->input->post('action_type') == "delete"){
				$result['data'] = $this->User_model->delete_users($this->input->post());
	    	}else{
	    		
		        $permissions = array(
		            'system_setup' 	=> 
		            	array (
		            	"0" 		=> $this->input->post('system_setup')
		            	),
		            'dashboard' 	=> 
		            	array (
		            	"0" 		=> $this->input->post('dashboard')
		            	),
		            'category' 		=> 
		            	array (
		            	"0" 		=> $this->input->post('category')
		            	),
		            'product' 		=> 
		            	array (
		            	"0" 		=> $this->input->post('product')
		            	),
		            'inventory' 	=> 
		            	array (
		            	"0" 		=> $this->input->post('inventory')
		            	),
		            'pos' 			=> 
		            	array (
		            	"0" 		=> $this->input->post('pos')
		            	),
		            'pending' 		=> 
		            	array (
		            	"0" 		=> $this->input->post('pending')
		            	),
		            'sales' 		=> 
		            	array (
		            	"0" 		=> $this->input->post('sales')
		            	),
		            'promo' 		=> 
		            	array (
		            	"0" 		=> $this->input->post('promo')
		            	),
		            'branch' 		=> 
		            	array (
		            	"0" 		=> $this->input->post('branch')
		            	),
		            'users' 		=> 
		            	array (
		            	"0" 		=> $this->input->post('users')
		            	),
		        );
	    		
		        $data = array(
		            'id' 			=> $this->input->post('id'),
		            'username' 		=> $this->input->post('username'),
		            'password' 		=> $this->input->post('password'),
		            'first_name' 	=> $this->input->post('first_name'),
		            'last_name' 	=> $this->input->post('last_name'),
		            'email' 		=> $this->input->post('email'),
		            'permissions'   => json_encode($permissions)
		        );

	    		if($this->input->post('action_type') == "create"){
	    			$check_if_exist			= $this->User_model->check_username($data);
	    			
	    			if(count($check_if_exist) > 0){
	    				$result['message'] 	= "Username already exist!";
	    			}else{
						$result['data'] 	= $this->User_model->add_users($data);
	    			}
		    	}else if($this->input->post('action_type') == "update"){
					$result['data'] = $this->User_model->update_users($data);
		    	}
	    	}
			
			header("Content-Type: application/json", true);
			$this->output->set_output(print(json_encode($result)));
			exit();
		}else{
            show_404();
        }
	}

	public function update_user_profile(){
		if($this->input->post()){
	    	
	    	$result['data']	= $this->User_model->update_user_profile($this->input->post());
			
			header("Content-Type: application/json", true);
			$this->output->set_output(print(json_encode($result)));
			exit();
		}else{
            show_404();
        }
	}

    public function activate_user()
    {
    	if($this->input->post()){
	    	
	    	$result['data']	= $this->User_model->activate_user($this->input->post());
			
			header("Content-Type: application/json", true);
			$this->output->set_output(print(json_encode($result)));
			exit();
		}else{
            show_404();
        }
	}

	public function reset_user_password()
	{
		if($this->input->post()){

			$result['user_info'] 	= $this->User_model->get_users($this->input->post('id'));

			$result['old_password'] = $this->User_model->get_old_password($this->input->post());

			if(array_check($result['old_password']) || ($result['user_info'][0]['password'] == sha1($this->input->post('password')))) {

				$response['status'] 		= false;
				$response['message']		= "Your new password can't be the same as you old password!";

				echo json_encode($response);

			}else{

				$result['user_info'] 	= $this->User_model->get_users($this->input->post('id'));

				$result['save_old'] 	= $this->User_model->save_old_password($result['user_info'][0]);

				$result['change_pass']	= $this->User_model->update_password($this->input->post());

				unset($result['user_info'][0]['password']);
				$this->session->set_userdata('pos_user_info', $result['user_info'][0]);

		       	$data['user_permissions']	= json_decode($result['user_info'][0]['permissions']);

		       	if ($data['user_permissions']->dashboard[0] != "0"){
					$response['message'] 	= base_url() . "dashboard";	
		       	}else if ($data['user_permissions']->pos[0] != "0"){
					$response['message'] 	= base_url() . "pos";	
		       	}else if ($data['user_permissions']->sales[0] != "0"){
					$response['message'] 	= base_url() . "sales";	
		       	}else if ($data['user_permissions']->pending[0] != "0"){
					$response['message'] 	= base_url() . "pending";	
		       	}else if ($data['user_permissions']->inventory[0] != "0"){
					$response['message'] 	= base_url() . "inventory";	
		       	}else{
					$response['message'] 	= base_url() . "profile";	
		       	}	
				
				$response['status'] 		= true;

				echo json_encode($response);

			}

		}else{
			show_404();
		}

	}

	public function change_password()
	{
		if($this->input->post()){
			$result['access']		= $this->session->userdata('pos_user_info');

			$data = array(
	            'id' 			=> $this->input->post($result['access']['id']),
	            'password' 		=> $this->input->post('confirm_password')
	        );

			$result['user_info'] 	= $this->User_model->get_users($result['access']['id']);

			$result['old_password'] = $this->User_model->get_old_password($data);

			if(array_check($result['old_password']) || ($result['user_info'][0]['password'] == sha1($this->input->post('password')))) {

				$response['status'] 		= false;
				$response['message']		= "Your new password can't be the same as you old password!";

				echo json_encode($response);

			}else{

				$result['user_info'] 	= $this->User_model->get_users($this->input->post('id'));

				$result['save_old'] 	= $this->User_model->save_old_password($result['user_info'][0]);

				$result['change_pass']	= $this->User_model->update_password($data);
				
				$response['status'] 		= true;
				$response['message']		= 'Successfully password changed';

				echo json_encode($response);

			}

		}else{
			show_404();
		}

	}
}
