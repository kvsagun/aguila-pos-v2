<div class="container-fluid">

  <div class="card">

    <div class="card-body">
    	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		    <h1 class="h2">Products</h1>
            <?php if($user_permissions->product[0] == 1 || $user_permissions->product[0] == 2 || $user_permissions->product[0] == 3): ?>
			    <div class="btn-toolbar mb-2 mb-md-0">
			        <button class="btn btn-success btn-add">Add Product</button>
			    </div>
            <?php endif;?>
		  </div>

		  <div class="col-lg-12">
		    <div class="row justify-content-between">
		      <div class="col-md-3">        
		        <div class="form-group row">
				    <label for="staticEmail" class="col-sm-5 col-form-label">Show</label>
				    <div class="col-sm-7">
				      <select class="form-control show-entries">
				      	<option>10</option>
				      	<option>50</option>
				      	<option>100</option>
				      </select>
				    </div>
				  </div>
		      </div>
		      <div class="col-md-3">        
		        <div class="form-group">
		            <input type="text" name="search" class="form-control search " placeholder="Search">
		        </div>
		      </div>
		    </div>

		    <div class="table-responsive">
			    <table class="table table-hover">
			      <thead>
			        <th>#</th>
			        <th>Category Name</th>
			        <th>Product Name</th>
			        <th>Description</th>
			        <th>Price</th>
			        <th>Taxable</th>
			        <th>Product Image</th>
			        <th>Quantity</th>
			        <th>Action</th>
			      </thead>

			        <tr class="table-list-template" style="display:none;">
			          <th class="count">1</th>
			          <td class="category-name">Biscuit</td>
			          <td class="product-name">Fita</td>
			          <td class="description">Fita Biscuit</td>
			          <td class="price">5.00</td>
			          <td><h4><span class="badge taxable-true badge-success"><i class="feather-20" data-feather="check"></i></span></h4><h4><span class="badge taxable-false badge-danger"><i class="feather-20" data-feather="x"></i></span></h4></td>
			          <td><img class="product-image img-thumbnail" src="" style="max-height: 75px; width: auto;"></td>
			          <td class="quantity">50</td>
			          <td>

            			<?php if($user_permissions->product[0] == 1 || $user_permissions->product[0] == 2 || $user_permissions->product[0] == 4): ?>
			            	<button class="btn btn-sm btn-primary edit"><span class="feather-16" data-feather="edit-2"></span></button>
            			<?php endif;?>	

            			<?php if($user_permissions->product[0] == 1): ?>
			            <button class="btn btn-sm btn-danger delete"><span class="feather-16" data-feather="trash-2"></span></button>
            			<?php endif;?>	

			          </td>
			        </tr>   

			      <tbody class="table-list">
			             
			      </tbody>

			    </table>
		    </div>

		    <li class="page-item pagination-template" style="display: none"><a class="page-link" href="#" offset="">1</a></li>

		    <div class="load-loader text-center" style="display:none;"><img src="<?php echo base_url() ?>assets/img/loader.gif"></div>
		    <div class="row justify-content-between">
		        <button class="btn btn-outline-info btn-previous"><span data-feather="chevron-left"></span></button>
		        <ul class="pagination">
		        </ul>
		        <button class="btn btn-outline-primary btn-next"><span data-feather="chevron-right"></span></button>
		    </div>
		  </div>

		  <br>
		  <br>

		  <div class="modal fade" id="createUpdateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-lg" role="document">
		      <div class="modal-content">
		        <div class="modal-header badge-primary">
		          <h5 class="modal-title" id="exampleModalLabel">Product</h5>
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		        </div>
		        <form id="_form">
		          <div class="modal-body">
		            <div class="col-12">
		                <input type="hidden" name="id" id="id">
		                <input type="hidden" value="add" name="action_type" id="action_type">
		                <div class="form-group">              
		                  <select class="form-control selectpicker" name="category_id" id="category_id" required data-live-search="true" title="Please Select Category">                    
		                    <?php foreach($categories as $val){
		                      echo "<option value='".$val['id']."'>".$val['name']."</option>";
		                    }?>

		                  </select>
		                </div>
		                <div class="form-group">              
		                  <input type="text" name="product_name" id="product_name" required="true" class="form-control" placeholder="Product Name">
		                </div>
		                <div class="form-group">
		                  <textarea name="description" id="description" class="form-control" placeholder="Description" rows="3" style="resize: none;"></textarea>
		                </div>
		                <div class="form-group">              
		                  <input type="number" step="0.01" min="0.01" name="price" id="price" required="true" class="form-control" placeholder="Price">
		                </div>
		                <div class="form-group">
		                  <div class="form-control">
		                    <input type="file" name="product_image" id="product_image" accept="image/x-png,image/jpeg">
		                    <small>Maximum image resulution (640x480)</small>
		                  </div>
		                </div>
		                <div class="form-group">              
		                  <input type="number" step="1" min="0" name="quantity" id="quantity" required="true" class="form-control" placeholder="Initial Quantity">
		                </div>
		                <div class="custom-control custom-checkbox my-1 mr-sm-2">
						    <input type="checkbox" class="custom-control-input" value="1" name="taxable" id="taxable">
						    <label class="custom-control-label" for="taxable">Taxable</label>
						</div>
		            </div>
		          </div>
		          <div class="modal-footer">
		            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
		            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
		          </div>
		        </form>
		      </div>
		    </div>
		  </div>
    </div>
    
  </div>

</div>