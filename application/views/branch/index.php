<div class="container-fluid">

  <div class="card">

    <div class="card-body">
    	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		    <h1 class="h2">Branches</h1>
            <?php if($user_permissions->branch[0] == 1 || $user_permissions->branch[0] == 2 || $user_permissions->branch[0] == 3): ?>
			    <div class="btn-toolbar mb-2 mb-md-0">
			        <button class="btn btn-success btn-add">Add Branch</button>
			    </div>
            <?php endif;?>
		  </div>

		  <div class="col-lg-12">
		    <div class="row justify-content-between">
		      <div class="col-md-3">        
		        <div class="form-group row">
				    <label for="staticEmail" class="col-sm-5 col-form-label">Show</label>
				    <div class="col-sm-7">
				      <select class="form-control show-entries">
				      	<option>10</option>
				      	<option>50</option>
				      	<option>100</option>
				      </select>
				    </div>
				  </div>
		      </div>
		      <div class="col-md-3">        
		        <div class="form-group">
		            <input type="text" name="search" class="form-control search " placeholder="Search">
		        </div>
		      </div>
		    </div>

		    <div class="table-responsive">
			    <table class="table table-hover">
			      <thead>
			        <th>#</th>
			        <th>Branch Name</th>
			        <th>Address</th>
			        <th>TIN</th>
			        <th>Contact Number</th>
			        <th>Action</th>
			      </thead>

			        <tr class="table-list-template" style="display:none;">
			          <th class="count">1</th>
			          <td class="branch-name">Red Line</td>
			          <td class="address">East Blue</td>
			          <td class="tin">12302017</td>
			          <td class="contact-number">jeussagun@gmail.com</td>
			          <td>

            			<?php if($user_permissions->branch[0] == 1 || $user_permissions->branch[0] == 2 || $user_permissions->branch[0] == 4): ?>
			            	<button class="btn btn-sm btn-primary edit"><span class="feather-16" data-feather="edit-2"></span></button>
            			<?php endif;?>	

            			<?php if($user_permissions->branch[0] == 1): ?>
			            <button class="btn btn-sm btn-danger delete"><span class="feather-16" data-feather="trash-2"></span></button>
            			<?php endif;?>	

			          </td>
			        </tr>   

			      <tbody class="table-list">
			             
			      </tbody>

			    </table>
		    </div>

		    <li class="page-item pagination-template" style="display: none"><a class="page-link" href="#" offset="">1</a></li>

		    <div class="load-loader text-center" style="display:none;"><img src="<?php echo base_url() ?>assets/img/loader.gif"></div>
		    <div class="row justify-content-between">
		        <button class="btn btn-outline-info btn-previous"><span data-feather="chevron-left"></span></button>
		        <ul class="pagination">
		        </ul>
		        <button class="btn btn-outline-primary btn-next"><span data-feather="chevron-right"></span></button>
		    </div>
		  </div>

		  <br>
		  <br>

		  <div class="modal fade" id="createUpdateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-lg" role="document">
		      <div class="modal-content">
		        <div class="modal-header badge-primary">
		          <h5 class="modal-title" id="exampleModalLabel">Branch</h5>
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		        </div>
		        <form id="_form">
		          <div class="modal-body">
		            <div class="col-12">
		                <input type="hidden" name="id" id="id">
		                <input type="hidden" value="create" name="action_type" id="action_type">
		                <div class="form-group">              
		                  <input type="text" name="branch_name" id="branch_name" required="true" class="form-control" placeholder="Branch Name">
		                </div>
		                <div class="form-group">
		                  <textarea name="address" id="address" class="form-control" placeholder="Address" rows="3" style="resize: none;"></textarea>
		                </div>
		                <div class="form-group">              
		                  <input type="text" name="tin" id="tin" required="true" class="form-control" placeholder="Tax Identifier Number">
		                </div>
		                <div class="form-group">              
		                  <input type="text" name="contact_number" id="contact_number" required="true" class="form-control" placeholder="Contact Number">
		                </div>
		            </div>
		          </div>
		          <div class="modal-footer">
		            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
		            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
		          </div>
		        </form>
		      </div>
		    </div>
		  </div>
    </div>
    
  </div>

</div>