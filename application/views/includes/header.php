<!DOCTYPE html>

<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="KVSAGUN POS - Restaurant Point of Sale">
    <meta name="author" content="Kenn Jeus Sagun - jeussagun@gmail.com">
    <meta name="keyword" content="POS, Point of Sale, Restaurant">
    <link rel="icon" href="<?= base_url();?>assets/img/favicon.ico">
    <title><?= $nav;?> - KVSAGUN POS</title>
    <!-- Icons-->
    <link href="<?= base_url();?>assets/fontawesome/css/fontawesome.min.css" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="<?= base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url();?>assets/css/custom.css" rel="stylesheet">
    <link href="<?= base_url();?>assets/css/pace.min.css" rel="stylesheet">
    <link href="<?= base_url();?>assets/css/sweetalert2.min.css" rel="stylesheet">

    <link href="<?= base_url();?>assets/css/bootstrap-select.min.css" rel="stylesheet">

    <?php if(isset($css) AND array_check($css)): ?>
      <?php foreach ($css as $css_value) : ?>
        <link href="<?= base_url() . "assets/css/" . $css_value?>" rel="stylesheet" type="text/css" />
      <?php endforeach;?>
    <?php endif;?>

    <script type="text/javascript">
      var base_url                = "<?= base_url();?>";
      var user_id                 = "<?= $access['id'];?>";
    </script>

  </head>
  <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    <header class="app-header navbar">
      <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <a class="navbar-brand" href="#">
        <img class="navbar-brand-full" src="<?= base_url();?>assets/img/kvsagun-pos-logo.png" width="89" height="25" alt="KVSAGUN POS Logo">
        <img class="navbar-brand-minimized" src="<?= base_url();?>assets/img/kvsagun-pos-logo.png" width="30" height="30" alt="KVSAGUN POS Logo">
      </a>
      <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
        <span class="navbar-toggler-icon"></span>
      </button>
      <ul class="nav navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
              <img class="img-avatar" src="<?= base_url();?>assets/img/profile-user.png" alt="admin@aguilapos.com">
            </a>
          </div>
          <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header text-center">
              <strong>Welcome, <span class="user-first-name"><?= $user_info[0]['first_name']; ?></span></strong>
            </div>
            <a class="dropdown-item" href="<?= base_url();?>profile">
              <i class="feather-16" data-feather="user"></i> Profile
            </a>
            <a class="dropdown-item change-password-link" href="#">
              <i class="feather-16" data-feather="lock"></i> Change Password
            </a>
            <a class="dropdown-item" href="<?= base_url();?>login/logout">
              <i class="feather-16" data-feather="log-out"></i> Logout</a>
          </div>
        </li>
      </ul>
    </header>
    <div class="app-body">
      <div class="sidebar">
        <nav class="sidebar-nav">
          <ul class="nav">

            <?php if($user_permissions->dashboard[0] != 0): ?>

            <li class="nav-item">
              <a class="nav-link" href="<?= base_url();?>dashboard">
                <i class="nav-icon" data-feather="home"></i> Dashboard
              </a>
            </li>

            <?php endif;?>

            <?php if($user_permissions->system_setup[0] != 0 ||  $user_permissions->category[0] != 0 ||  $user_permissions->product[0] != 0 ||  $user_permissions->branch[0] != 0 || $user_permissions->users[0] != 0): ?>

            <li class="nav-item nav-dropdown">
              <a class="nav-link nav-dropdown-toggle" href="#">
                <i class="nav-icon" data-feather="settings"></i> Setup</a>
              <ul class="nav-dropdown-items">

                <?php if($user_permissions->system_setup[0] != 0): ?>
                  <li class="nav-item">
                    <a class="nav-link" href="<?= base_url();?>general">
                      <i class="nav-icon"></i> General</a>
                  </li>
                <?php endif;?>

                <?php if($user_permissions->branch[0] != 0): ?>
                  <li class="nav-item">
                    <a class="nav-link" href="<?= base_url();?>branch">
                      <i class="nav-icon"></i> Branch</a>
                  </li>
                <?php endif;?>

                <?php if($user_permissions->category[0] != 0): ?>
                  <li class="nav-item">
                    <a class="nav-link" href="<?= base_url();?>category">
                      <i class="nav-icon"></i> Category</a>
                  </li>
                <?php endif;?>

                <?php if($user_permissions->product[0] != 0): ?>
                  <li class="nav-item">
                    <a class="nav-link" href="<?= base_url();?>product">
                      <i class="nav-icon"></i> Product</a>
                  </li>
                <?php endif;?>

                <?php if($user_permissions->users[0] != 0): ?>
                  <li class="nav-item">
                    <a class="nav-link" href="<?= base_url();?>users">
                      <i class="nav-icon"></i> Users</a>
                  </li>
                <?php endif;?>
              </ul>
            </li>

            <?php endif;?>

            <?php if($user_permissions->pos[0] != 0 ||  $user_permissions->pending[0] != 0 ||  $user_permissions->sales[0] != 0): ?>
              <li class="nav-item nav-dropdown">
                <a class="nav-link nav-dropdown-toggle" href="#">
                  <i class="nav-icon" data-feather="shopping-cart"></i> POS</a>
                <ul class="nav-dropdown-items">

                  <?php if($user_permissions->pos[0] != 0): ?>
                  <li class="nav-item">
                    <a class="nav-link" href="<?= base_url();?>pos">
                      <i class="nav-icon"></i> Point of Sales</a>
                  </li>
                  <?php endif;?>
                  
                  <?php if($user_permissions->pending[0] != 0): ?>
                  <li class="nav-item">
                    <a class="nav-link" href="<?= base_url();?>pending">
                      <i class="nav-icon"></i> Pending Orders</a>
                  </li>
                  <?php endif;?>

                  <?php if($user_permissions->sales[0] != 0): ?>
                    <li class="nav-item">
                      <a class="nav-link" href="<?= base_url();?>sales">
                        <i class="nav-icon"></i> Sales</a>
                    </li>
                  <?php endif;?>

                </ul>
              </li>
            <?php endif;?>

            <?php if($user_permissions->inventory[0] != 0): ?>
              <li class="nav-item">
                <a class="nav-link" href="<?= base_url();?>inventory">
                  <i class="nav-icon" data-feather="plus"></i> Inventory
                </a>
              </li>
            <?php endif;?>

          </ul>
        </nav>
      </div>
      <main class="main">

        <br/>