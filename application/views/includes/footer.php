

      <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header badge-info">
              <h5 class="modal-title" id="exampleModalLabel">Confirm</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
              <div class="modal-body">
                <div class="col-12">
                    <span class="confirm-message"></span>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-sm btn-primary btn-confirm-delete" id="confirm">Submit</button>
              </div>
          </div>
        </div>
      </div>

      </main>
    </div>
    <footer class="app-footer">
      <div>
        <a href="#">KVSAGUN POS</a>
        <span>&copy; 2019 kvsagun.</span>
      </div>
    </footer>

    <script src="<?= base_url();?>assets/js/feather.min.js"></script>
    <script>
      feather.replace();
    </script>

    <script src="<?= base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?= base_url();?>assets/js/sweetalert2.min.js"></script>
    
    <script src="<?= base_url();?>assets/js/popper.min.js"></script>
    <script src="<?= base_url();?>assets/fontawesome/js/fontawesome.min.js"></script>
    <script src="<?= base_url();?>assets/js/pace.min.js"></script>
    <script src="<?= base_url();?>assets/js/perfect-scrollbar.min.js"></script>
    <script src="<?= base_url();?>assets/js/coreui.min.js"></script>
    <script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url();?>assets/js/bootstrap-select.min.js" type="text/javascript"></script>

    <script src="<?= base_url();?>assets/js/modules/basic.js"></script>

    <?php if(isset($javascripts) AND array_check($javascripts)): ?>
      <?php foreach ($javascripts as $javascript) : ?>
        <?php if($javascript != ''):?>
          <script src="<?= base_url() . "assets/js/" . $javascript?>"></script>
        <?php endif;?>
      <?php endforeach;?>
    <?php endif;?>
  </body>
</html>
