<div class="container-fluid">

  <div class="card">

    <div class="card-body">
    	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		    <h1 class="h2">General</h1>
		  </div>

		  <div class="col-lg-12">

            <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#company" role="tab" aria-controls="company">Company Details</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#senior" role="tab" aria-controls="senior">Senior Discount</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#vat" role="tab" aria-controls="vat">Value Added Tax</a>
              </li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="company" role="tabpanel">

	            <?php if($user_permissions->system_setup[0] == 1): ?>
				    <div class="text-right">
		        		<button class="btn btn-sm btn-primary edit-company"><span class="feather-16" data-feather="edit-2"></span></button>
				  	</div>
	            <?php endif;?>

			  	<div class="row" style="margin-top: 10px;">
			  		<div class="col-sm-3">
			  			<strong>Logo: </strong>
			  		</div>
			  		<div class="col-sm-9">
			  			<img class="img-thumbnail company-logo" src="" alt="company-logo.png" style="max-height: 75px;">
			  		</div>
			  	</div>

              	<div class="row">
			  		<div class="col-sm-3">
			  			<strong>Company Name: </strong>
			  		</div>
			  		<div class="col-sm-9">
			  			<span class="company-name"></span>
			  		</div>
			  	</div>

              </div>
              <div class="tab-pane" id="senior" role="tabpanel">

	            <?php if($user_permissions->system_setup[0] == 1): ?>
				    <div class="text-right">
		        		<button class="btn btn-sm btn-primary edit-senior"><span class="feather-16" data-feather="edit-2"></span></button>
				  	</div>
	            <?php endif;?>

			  	<div class="row" style="margin-top: 10px;">
			  		<div class="col-sm-3">
			  			<strong>Senior Discount: </strong>
			  		</div>
			  		<div class="col-sm-9">
			  			<span class="senior-discount">12%</span>
			  		</div>
			  	</div>
			  	              	
              </div>
              <div class="tab-pane" id="vat" role="tabpanel">

	            <?php if($user_permissions->system_setup[0] == 1): ?>
				    <div class="text-right">
		        		<button class="btn btn-sm btn-primary edit-vat"><span class="feather-16" data-feather="edit-2"></span></button>
				  	</div>
	            <?php endif;?>

			  	<div class="row" style="margin-top: 10px;">
			  		<div class="col-sm-3">
			  			<strong>VAT Percentage: </strong>
			  		</div>
			  		<div class="col-sm-9">
			  			<span class="vat">12%</span>
			  		</div>
			  	</div>
			  	              	
              </div>
            </div>

		  </div>

		  <div class="modal fade" id="updateCompanyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-lg" role="document">
		      <div class="modal-content">
		        <div class="modal-header badge-primary">
		          <h5 class="modal-title" id="exampleModalLabel">Company Details</h5>
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		        </div>
		        <form id="_company_form">
		          <div class="modal-body">
		            <div class="col-12">
		                <input type="hidden" name="id" id="company_id">
		                <div class="form-group">              
		                  <input type="text" name="name" id="company_name" required="true" class="form-control" placeholder="Company Name">
		                </div>
		                <div class="form-group">
		                  <div class="form-control">
		                    <input type="file" name="logo" id="company_logo" accept="image/x-png,image/jpeg">
		                    <small>Maximum image resulution (640x480)</small>
		                  </div>
		                </div>
		            </div>
		          </div>
		          <div class="modal-footer">
		            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
		            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
		          </div>
		        </form>
		      </div>
		    </div>
		  </div>

		  <div class="modal fade" id="updateSeniorModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-lg" role="document">
		      <div class="modal-content">
		        <div class="modal-header badge-primary">
		          <h5 class="modal-title" id="exampleModalLabel">Senior Discount</h5>
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		        </div>
		        <form id="_senior_form">
		          <div class="modal-body">
		            <div class="col-12">
		                <input type="hidden" name="id" id="senior_id">
		                <div class="form-group">              
		                  <input type="number" step="0.25" min="0" name="discount" id="senior_discount" required="true" class="form-control" placeholder="xx.xx%">
		                </div>
		            </div>
		          </div>
		          <div class="modal-footer">
		            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
		            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
		          </div>
		        </form>
		      </div>
		    </div>
		  </div>

		  <div class="modal fade" id="updateVatModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-lg" role="document">
		      <div class="modal-content">
		        <div class="modal-header badge-primary">
		          <h5 class="modal-title" id="exampleModalLabel">Value Added Tax</h5>
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		        </div>
		        <form id="_vat_form">
		          <div class="modal-body">
		            <div class="col-12">
		                <input type="hidden" name="id" id="vat_id">
		                <div class="form-group">              
		                  <input type="number" step="0.25" min="0" name="tax" id="tax_percent" required="true" class="form-control" placeholder="xx.xx%">
		                </div>
		            </div>
		          </div>
		          <div class="modal-footer">
		            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
		            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
		          </div>
		        </form>
		      </div>
		    </div>
		  </div>
    </div>
    
  </div>

</div>