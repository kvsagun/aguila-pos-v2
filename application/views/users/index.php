<div class="container-fluid">

  <div class="card">

    <div class="card-body">
    	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		    <h1 class="h2">Users</h1>
            <?php if($user_permissions->users[0] == 1 || $user_permissions->users[0] == 2 || $user_permissions->users[0] == 3): ?>
			    <div class="btn-toolbar mb-2 mb-md-0">
			        <button class="btn btn-success btn-add">Add User</button>
			    </div>
            <?php endif;?>
		  </div>

		  <div class="col-lg-12">
		    <div class="row justify-content-between">
		      <div class="col-md-3">        
		        <div class="form-group row">
				    <label for="staticEmail" class="col-sm-5 col-form-label">Show</label>
				    <div class="col-sm-7">
				      <select class="form-control show-entries">
				      	<option>10</option>
				      	<option>50</option>
				      	<option>100</option>
				      </select>
				    </div>
				  </div>
		      </div>
		      <div class="col-md-3">        
		        <div class="form-group">
		            <input type="text" name="search" class="form-control search " placeholder="Search">
		        </div>
		      </div>
		    </div>

		    <div class="table-responsive">
			    <table class="table table-hover">
			      <thead>
			        <th>#</th>
			        <th>Name</th>
			        <th>Username</th>
			        <th>Users</th>
			        <th>Status</th>
			        <th>Last Login</th>
			        <th>Action</th>
			      </thead>

			        <tr class="table-list-template" style="display:none;">
			          <th class="count">1</th>
			          <td class="full-name">Sagun, Kenn Jeus</td>
			          <td class="username">kvsagun</td>
			          <td class="email">admin</td>
			          <td><h4><span class="badge status-true badge-success"><i class="feather-20" data-feather="check"></i></span></h4><h4><span class="badge status-false badge-danger"><i class="feather-20" data-feather="x"></i></span></h4></td>
			          <td class="last-login">December 30 2017</td>
			          <td class="action">

		            	<button class="btn btn-sm btn-primary user-activate" style="display: none;"><span class="feather-16" data-feather="user-check"></span></button>

            			<?php if($user_permissions->users[0] == 1 || $user_permissions->users[0] == 2 || $user_permissions->users[0] == 4): ?>
			            	<button class="action-btn btn btn-sm btn-primary edit"><span class="feather-16" data-feather="edit-2"></span></button>

			            	<button class="action-btn btn btn-sm btn-success user-permissions"><span class="feather-16" data-feather="eye"></span></button>

			            	<button class="action-btn btn btn-sm btn-warning user-change-password"><span class="feather-16" data-feather="lock"></span></button>
            			<?php endif;?>	

            			<?php if($user_permissions->users[0] == 1): ?>
			            <button class="action-btn btn btn-sm btn-danger delete"><span class="feather-16" data-feather="trash-2"></span></button>
            			<?php endif;?>	

			          </td>
			        </tr>   

			      <tbody class="table-list">
			             
			      </tbody>

			    </table>
		    </div>

		    <li class="page-item pagination-template" style="display: none"><a class="page-link" href="#" offset="">1</a></li>

		    <div class="load-loader text-center" style="display:none;"><img src="<?php echo base_url() ?>assets/img/loader.gif"></div>
		    <div class="row justify-content-between">
		        <button class="btn btn-outline-info btn-previous"><span data-feather="chevron-left"></span></button>
		        <ul class="pagination">
		        </ul>
		        <button class="btn btn-outline-primary btn-next"><span data-feather="chevron-right"></span></button>
		    </div>
		  </div>

		  <br>
		  <br>

		  <div class="modal fade" id="createUpdateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-lg" role="document">
		      <div class="modal-content">
		        <div class="modal-header badge-primary">
		          <h5 class="modal-title" id="exampleModalLabel">User</h5>
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		        </div>
		        <form id="_form">
		          <div class="modal-body">
		            <div class="col-12">
		                <input type="hidden" name="id" id="id">
		                <input type="hidden" value="create" name="action_type" id="action_type">
              			<div id="form-message" class="text-center"></div>  
		                <div class="row">
		                	<div class="col-md-4">
				                <div class="form-group">              
				                  <input type="text" name="first_name" id="first_name" required="true" class="form-control" placeholder="First Name">
				                </div>
		                	</div>

		                	<div class="col-md-4">
				                <div class="form-group">              
				                  <input type="text" name="last_name" id="last_name" required="true" class="form-control" placeholder="Last Name">
				                </div>
		                	</div>

		                	<div class="col-md-4">
				                <div class="form-group">
				                  <input type="email" name="email" id="email" required="true" class="form-control" placeholder="Email">
				                </div>
		                	</div>
		                </div>
		                <div class="row">
		                	<div class="col-md-6">
				                <div class="form-group">
				                  <input type="text" name="username" id="username" required="true" class="form-control" placeholder="Username">
				                </div>
		                	</div>
		                	
		                	<div class="col-md-6">
				                <div class="form-group">
				                  <input type="password" name="password" id="password" required="true" class="form-control" placeholder="Password">
				                </div>
		                	</div>
		                </div>

		                <div class="table-responsive">

			                <table class="table table-hover">
			                	<thead>
							        <th>Module</th>
							        <th>Permission</th>
			                	</thead>
			                	<tbody>
			                		<tr>
			                			<td>General <br><small>(Company Details, Tax, Senior Discount)</small></td>
			                			<td>
			                				<select class="form-control selectpicker permission-system_setup" name="system_setup">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">View and Update</option>
			                					<option value="2">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Dashboard</td>
			                			<td>
			                				<select class="form-control selectpicker permission-dashboard" name="dashboard">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Category</td>
			                			<td>
			                				<select class="form-control selectpicker permission-category" name="category">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">Add, Update, Delete and View</option>
			                					<option value="2">Add, Update and View</option>
			                					<option value="3">Add and View</option>
			                					<option value="4">Update and View</option>
			                					<option value="5">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Product</td>
			                			<td>
			                				<select class="form-control selectpicker permission-product" name="product">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">Add, Update, Delete and View</option>
			                					<option value="2">Add, Update and View</option>
			                					<option value="3">Add and View</option>
			                					<option value="4">Update and View</option>
			                					<option value="5">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Branch</td>
			                			<td>
			                				<select class="form-control selectpicker permission-branch" name="branch">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">Add, Update, Delete and View</option>
			                					<option value="2">Add, Update and View</option>
			                					<option value="3">Add and View</option>
			                					<option value="4">Update and View</option>
			                					<option value="5">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Users</td>
			                			<td>
			                				<select class="form-control selectpicker permission-users" name="users">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">Add, Update, Delete and View</option>
			                					<option value="2">Add, Update and View</option>
			                					<option value="3">Add and View</option>
			                					<option value="4">Update and View</option>
			                					<option value="5">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Promo</td>
			                			<td>
			                				<select class="form-control selectpicker permission-promo" name="promo">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">Add, Update, Delete and View</option>
			                					<option value="2">Add, Update and View</option>
			                					<option value="3">Add and View</option>
			                					<option value="4">Update and View</option>
			                					<option value="5">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Point of Sale</td>
			                			<td>
			                				<select class="form-control selectpicker permission-pos" name="pos">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">Add</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Peding Order</td>
			                			<td>
			                				<select class="form-control selectpicker permission-pending" name="pending">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">View and Update</option>
			                					<option value="2">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Sales</td>
			                			<td>
			                				<select class="form-control selectpicker permission-sales" name="sales">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">View and Update</option>
			                					<option value="2">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Inventory</td>
			                			<td>
			                				<select class="form-control selectpicker permission-inventory" name="inventory">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">Add and View</option>
			                					<option value="2">View Only</option>
			                				</select>
			                			</td>
			                		</tr>
			                	</tbody>
			                </table>
		                	
		                </div>
		            </div>
		          </div>
		          <div class="modal-footer">
		            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
		            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
		          </div>
		        </form>
		      </div>
		    </div>
		  </div>


		  <div class="modal fade" id="viewUserPermissionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-lg" role="document">
		      <div class="modal-content">
		        <div class="modal-header badge-primary">
		          <h5 class="modal-title" id="exampleModalLabel">User's Permissions</h5>
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		        </div>
		          <div class="modal-body">
		            <div class="col-12">
		                <div class="table-responsive">

			                <table class="table table-hover">
			                	<thead>
							        <th>Module</th>
							        <th>Permission</th>
			                	</thead>
			                	<tbody>
			                		<tr>
			                			<td>General <br><small>(Company Details, Tax, Senior Discount)</small></td>
			                			<td>
			                				<select class="form-control selectpicker permission-system_setup" name="system_setup" disabled="true">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">View and Update</option>
			                					<option value="2">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Dashboard</td>
			                			<td>
			                				<select class="form-control selectpicker permission-dashboard" name="dashboard" disabled="true">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Category</td>
			                			<td>
			                				<select class="form-control selectpicker permission-category" name="category" disabled="true">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">Add, Update, Delete and View</option>
			                					<option value="2">Add, Update and View</option>
			                					<option value="3">Add and View</option>
			                					<option value="4">Update and View</option>
			                					<option value="5">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Product</td>
			                			<td>
			                				<select class="form-control selectpicker permission-product" name="product" disabled="true">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">Add, Update, Delete and View</option>
			                					<option value="2">Add, Update and View</option>
			                					<option value="3">Add and View</option>
			                					<option value="4">Update and View</option>
			                					<option value="5">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Branch</td>
			                			<td>
			                				<select class="form-control selectpicker permission-branch" name="branch" disabled="true">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">Add, Update, Delete and View</option>
			                					<option value="2">Add, Update and View</option>
			                					<option value="3">Add and View</option>
			                					<option value="4">Update and View</option>
			                					<option value="5">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Users</td>
			                			<td>
			                				<select class="form-control selectpicker permission-users" name="users" disabled="true">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">Add, Update, Delete and View</option>
			                					<option value="2">Add, Update and View</option>
			                					<option value="3">Add and View</option>
			                					<option value="4">Update and View</option>
			                					<option value="5">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Promo</td>
			                			<td>
			                				<select class="form-control selectpicker permission-promo" name="promo" disabled="true">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">Add, Update, Delete and View</option>
			                					<option value="2">Add, Update and View</option>
			                					<option value="3">Add and View</option>
			                					<option value="4">Update and View</option>
			                					<option value="5">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Point of Sale</td>
			                			<td>
			                				<select class="form-control selectpicker permission-pos" name="pos" disabled="true">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">Add</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Peding Order</td>
			                			<td>
			                				<select class="form-control selectpicker permission-pending" name="pending" disabled="true">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">View and Update</option>
			                					<option value="2">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Sales</td>
			                			<td>
			                				<select class="form-control selectpicker permission-sales" name="sales" disabled="true">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">View and Update</option>
			                					<option value="2">View Only</option>
			                				</select>
			                			</td>
			                		</tr>

			                		<tr>
			                			<td>Inventory</td>
			                			<td>
			                				<select class="form-control selectpicker permission-inventory" name="inventory" disabled="true">
			                					<option value="0" selected="true">No Access</option>
			                					<option value="1">Add and View</option>
			                					<option value="2">View Only</option>
			                				</select>
			                			</td>
			                		</tr>
			                	</tbody>
			                </table>
		                	
		                </div>
		            </div>
		          </div>
		      </div>
		    </div>
		  </div>
    </div>
    
  </div>

</div>