<div class="container-fluid">

  <div class="card">

    <div class="card-body">
    	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
		    <h1 class="h2">Profile</h1>
		</div>

		  <div class="col-lg-12" id="users_data">
		  	<div class="text-right">
        		<button class="btn btn-sm btn-primary edit"><span class="feather-16" data-feather="edit-2"></span></button>
		  	</div>

		  	<div class="row">
		  		<div class="col-sm-3">
		  			<strong>Username: </strong>
		  		</div>
		  		<div class="col-sm-9">
		  			<span class="username"></span>
		  		</div>
		  	</div>

		  	<div class="row" style="margin-top: 10px;">
		  		<div class="col-sm-3">
		  			<strong>Email: </strong>
		  		</div>
		  		<div class="col-sm-9">
		  			<span class="email"></span>
		  		</div>
		  	</div>

		  	<div class="row" style="margin-top: 10px;">
		  		<div class="col-sm-3">
		  			<strong>First Name: </strong>
		  		</div>
		  		<div class="col-sm-9">
		  			<span class="first-name"></span>
		  		</div>
		  	</div>

		  	<div class="row" style="margin-top: 10px;">
		  		<div class="col-sm-3">
		  			<strong>Last Name: </strong>
		  		</div>
		  		<div class="col-sm-9">
		  			<span class="last-name"></span>
		  		</div>
		  	</div>
		    
		  </div>

		  <div class="modal fade" id="createUpdateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-lg" role="document">
		      <div class="modal-content">
		        <div class="modal-header badge-primary">
		          <h5 class="modal-title" id="exampleModalLabel">User</h5>
		          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true">&times;</span>
		          </button>
		        </div>
		        <form id="_form">
		          <div class="modal-body">
		            <div class="col-12">
		                <input type="hidden" name="id" id="id">
		                <input type="hidden" value="update" name="action_type" id="action_type">
              			<div id="form-message" class="text-center"></div>  
		                <div class="row">
		                	<div class="col-md-12">
				                <div class="form-group">
				                  <input type="text" name="username" id="username" readonly="true" class="form-control" placeholder="Username">
				                </div>
		                	</div>
		                </div>
		                <div class="row">
		                	<div class="col-md-4">
				                <div class="form-group">              
				                  <input type="text" name="first_name" id="first_name" required="true" class="form-control" placeholder="First Name">
				                </div>
		                	</div>

		                	<div class="col-md-4">
				                <div class="form-group">              
				                  <input type="text" name="last_name" id="last_name" required="true" class="form-control" placeholder="Last Name">
				                </div>
		                	</div>

		                	<div class="col-md-4">
				                <div class="form-group">
				                  <input type="email" name="email" id="email" required="true" class="form-control" placeholder="Email">
				                </div>
		                	</div>
		                </div>

		            </div>
		          </div>
		          <div class="modal-footer">
		            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
		            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
		          </div>
		        </form>
		      </div>
		    </div>
		  </div>
		
    </div>
    
  </div>

</div>