<!DOCTYPE html>

<html lang="en">
  <head>
    <base href="./">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="description" content="KVSAGUN POS - Restaurant Point of Sale">
    <meta name="author" content="Kenn Jeus Sagun - jeussagun@gmail.com">
    <meta name="keyword" content="POS, Point of Sale, Restaurant">
    <link rel="icon" href="<?= base_url();?>assets/img/favicon.ico">
    <title><?= $nav;?> - KVSAGUN POS</title>
    <!-- Icons-->
    <link href="<?= base_url();?>assets/fontawesome/css/fontawesome.min.css" rel="stylesheet">
    <!-- Main styles for this application-->
    <link href="<?= base_url();?>assets/css/style.css" rel="stylesheet">
    <link href="<?= base_url();?>assets/css/custom.css" rel="stylesheet">
    <link href="<?= base_url();?>assets/css/pace.min.css" rel="stylesheet">
    <link href="<?= base_url();?>assets/css/sweetalert2.min.css" rel="stylesheet">

    <script type="text/javascript">
      var base_url = "<?= base_url();?>";
    </script>

  </head>
  <body class="app flex-row align-items-center" style="background-color: #4DB6AC;">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-6">
          <div class="card-group">
            <div class="card p-4">
              <div class="card-body">
                <?php if($info['user_id'] != ''): ?>
                  <form id="_form">
                    <div class="text-center">
                      <img src="<?php echo base_url();?>assets/img/kvsagun-pos-logo.png" class="img-fluid" alt="Responsive image" style="max-width: 50%; padding-bottom: 10px; margin: auto;">
                      <p class="text-muted">Reset Password</p>
                    </div>
                    <input type="hidden" name="id" value="<?= $info['user_id']?>">
                    <div class="input-group mb-4">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="feather-16" data-feather="lock"></i>
                        </span>
                      </div>
                      <input class="form-control password" type="password" name="password" required="true" placeholder="Password">
                    </div>
                    <div class="input-group mb-4">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="feather-16" data-feather="lock"></i>
                        </span>
                      </div>
                      <input class="form-control confirm-password" type="password" name="confirm_password" required="true" placeholder="Confirm Password">
                    </div>
                    <div id="login-message" class="text-center"></div>  
                    <div class="row">
                      <div class="col-12">
                        <button class="btn btn-block btn-primary px-4" type="submit">Change Password</button>
                      </div>
                    </div>
                  </form>
                <?php endif;?>
                <?php if($info['user_id'] == ''): ?>
                  <span>This reset password link is no longer active.</span>
                <?php endif;?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <script src="<?= base_url();?>assets/js/feather.min.js"></script>
    <script>
      feather.replace();
    </script>

    <script src="<?= base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?= base_url();?>assets/js/sweetalert2.min.js"></script>

    <script src="<?= base_url();?>assets/js/modules/reset_password.js"></script>

    <script src="<?= base_url();?>assets/js/popper.min.js"></script>
    <script src="<?= base_url();?>assets/fontawesome/js/fontawesome.min.js"></script>
    <script src="<?= base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url();?>assets/js/pace.min.js"></script>
    <script src="<?= base_url();?>assets/js/perfect-scrollbar.min.js"></script>
    <script src="<?= base_url();?>assets/js/coreui.min.js"></script>
  </body>
</html>
