<?php

class Login_model extends CI_MODEL{

	/****************************************/
	/****************************************/
	/*			 DEVELOPED BY:	 			*/
	/*			 KENN JEUS SAGUN 			*/
	/****************************************/
	/****************************************/

	function authorize_user($data)
	{
		$sql = "SELECT
				  u.id, 
				  u.`first_name`, 
				  u.`last_name`, 
				  u.permissions,
				  u.email,
				  u.`username`, 
				  u.`permissions`, 
				  u.is_deleted
				FROM
				  users u
				 WHERE u.username = '" . $data['username'] . "'
				 AND u.password = sha1('" . $data['password'] . "')";

        $result = $this->db->query($sql);
        return $result->row_array();
	}

	function update_last_login($id)
	{
		$sql = "UPDATE users
				SET last_login = NOW()
				WHERE id = $id";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function check_forgot_password_email($data)
	{
		$sql = "SELECT
				  u.id, 
				  u.`first_name`, 
				  u.`last_name`, 
				  u.permissions,
				  u.email,
				  u.`username`, 
				  u.`permissions`, 
				  u.is_deleted
				FROM
				  users u
				 WHERE u.email = '" . $data['email'] . "'";

        $result = $this->db->query($sql);
        return $result->row_array();
	}

	function check_forgot_password_token($token)
	{
		$sql = "SELECT
					*
				FROM
				  forgot_password_token
				 WHERE token = '" . $token . "' 
				 	AND valid_until >= NOW()";

        $result = $this->db->query($sql);
        return $result->row_array();
	}

	function save_forgot_password_token($data)
	{
		$sql = "INSERT INTO forgot_password_token(
					user_id, 
					token, 
					valid_until)
				VALUES(
				'".$data['user_id']."', 
				'".$data['token']."', 
				DATE_ADD(NOW(), INTERVAL 30 MINUTE)
			)";

        $this->db->query($sql);
        return $this->db->insert_id();
	}

}
?>