<?php

class General_model extends CI_MODEL{

	/****************************************/
	/****************************************/
	/*			 DEVELOPED BY:	 			*/
	/*			 KENN JEUS SAGUN 			*/
	/****************************************/
	/****************************************/

	function update_company($data, $logo)
	{
		$sql = "UPDATE company_details
				SET name 			= '".$data['name']."',
					" . (($logo != "" ) ? "logo = '" . $logo . "'," : "") . "
					date_modified 	= NOW()
				WHERE id 			= '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function get_company($id = 0)
	{
		$where_query = '';

		if($id != 0){
			$where_query .= " AND id = " . $id;
		}

		$sql = "SELECT
				  *
				FROM
				  company_details
				WHERE 1 = 1 " . $where_query . "";

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function update_senior($data)
	{
		$sql = "UPDATE senior_discount
				SET discount 		= '".$data['discount']."',
					date_modified 	= NOW()
				WHERE id 			= '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function get_senior($id = 0)
	{
		$where_query = '';

		if($id != 0){
			$where_query .= " AND id = " . $id;
		}

		$sql = "SELECT
				  *
				FROM
				  senior_discount
				WHERE 1 = 1 " . $where_query . "";

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function update_vat($data)
	{
		$sql = "UPDATE vat
				SET tax 			= '".$data['tax']."',
					date_modified 	= NOW()
				WHERE id 			= '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function get_vat($id = 0)
	{
		$where_query = '';

		if($id != 0){
			$where_query .= " AND id = " . $id;
		}

		$sql = "SELECT
				  *
				FROM
				  vat
				WHERE 1 = 1 " . $where_query . "";

        $result = $this->db->query($sql);
        return $result->result_array();
	}


}
?>