<?php

class Category_model extends CI_MODEL{

	/****************************************/
	/****************************************/
	/*			 DEVELOPED BY:	 			*/
	/*			 KENN JEUS SAGUN 			*/
	/****************************************/
	/****************************************/

	function add_categories($data)
	{
		$sql = "INSERT INTO category(
					name, 
					description)
				VALUES(
				'".$data['category_name']."', 
				'".$data['description']."')";

        $this->db->query($sql);
        return $this->db->insert_id();
	}

	function update_categories($data)
	{
		$sql = "UPDATE category
				SET name 			= '".$data['category_name']."',
					description 	= '".$data['description']."',
					date_modified 	= NOW()
				WHERE id = '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function delete_categories($data)
	{
		$sql = "UPDATE category
				SET is_deleted = 1
				WHERE id = '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function get_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    name LIKE '%" . $search . "%'
				    OR
				    description LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `category`
				WHERE is_deleted = 0 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_categories($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    name LIKE '%" . $search . "%'
				    OR
				    description LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT
				  *
				FROM
				  category
				WHERE is_deleted = 0 " . $where_query . "
				ORDER BY (
				    CASE
				      WHEN date_modified > date_created
				      THEN date_modified
				      ELSE date_created
				    END
				  ) DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}


}
?>