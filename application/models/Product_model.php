<?php

class Product_model extends CI_MODEL{

	/****************************************/
	/****************************************/
	/*			 DEVELOPED BY:	 			*/
	/*			 KENN JEUS SAGUN 			*/
	/****************************************/
	/****************************************/

	function add_product($data, $product_image)
	{
		$sql = "INSERT INTO product(
					category_id, 
					name, 
					description, 
					price, 
					quantity 
					" . ((isset($data['taxable'])) ? ", taxable" : "") . " 
					" . (($product_image != "" ) ? ", product_image" : "") . "
					)
				VALUES(
				'".$data['category_id']."', 
				'".$data['product_name']."', 
				'".$data['description']."', 
				'".$data['price']."', 
				'".$data['quantity']."'
				" . ((isset($data['taxable'])) ? ", '" . $data['taxable'] . "'" : "") . " 
				" . (($product_image != "" ) ? ", '" . $product_image . "'" : "") . "
			)";

        $this->db->query($sql);
        return $this->db->insert_id();
	}

	function update_product($data, $product_image)
	{
		$sql = "UPDATE product
				SET category_id 	= '".$data['category_id']."',
					name 			= '".$data['product_name']."',
					price 			= '".$data['price']."',
					description 	= '".$data['description']."',
					taxable 		= '" . ((isset($data['taxable'])) ? '1' : '0') . "',
					" . (($product_image != "" ) ? "product_image = '" . $product_image . "'," : "") . "
					date_modified 	= NOW()
				WHERE id 			= '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function delete_product($data)
	{
		$sql = "UPDATE product
				SET is_deleted = 1
				WHERE id = '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function get_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    p.name LIKE '%" . $search . "%'
				    OR
				    c.name LIKE '%" . $search . "%'
				    OR
				    p.description LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(p.id) AS total_count, 
					(
						COUNT(p.id) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `product` p
				INNER JOIN category c 
				  ON c.`id` = p.`category_id` 
				WHERE p.is_deleted = 0 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_product($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND p.id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    c.name LIKE '%" . $search . "%' 
				    OR p.name LIKE '%" . $search . "%'
				    OR p.description LIKE '%" . $search . "%'
				  ) ";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT 
				  p.`id`,
				  p.`category_id`,
				  c.`name` as category_name,
				  p.`name`,
				  p.`description`,
				  p.`price`,
				  p.`quantity`,
				  p.`taxable`,
				  p.`product_image`
				FROM
				  product p 
				  INNER JOIN category c 
				    ON c.`id` = p.`category_id` 
				WHERE p.is_deleted = 0 
				  AND c.`is_deleted` = 0 " . $where_query . "
				ORDER BY (
				    CASE
				      WHEN p.date_modified > p.date_created
				      THEN p.date_modified
				      ELSE p.date_created
				    END
				  ) DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}


}
?>