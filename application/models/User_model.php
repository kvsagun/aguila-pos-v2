<?php

class User_model extends CI_MODEL{

	/****************************************/
	/****************************************/
	/*			 DEVELOPED BY:	 			*/
	/*			 KENN JEUS SAGUN 			*/
	/****************************************/
	/****************************************/

	function add_users($data)
	{
		$sql = "INSERT INTO users(
					first_name, 
					last_name, 
					email, 
					username, 
					password,
					permissions)
				VALUES(
				'".$data['first_name']."', 
				'".$data['last_name']."', 
				'".$data['email']."', 
				'".$data['username']."', 
				SHA1('".$data['password']."'), 
				'".$data['permissions']."')";

        $this->db->query($sql);
        return $this->db->insert_id();
	}

	function update_users($data)
	{
		$sql = "UPDATE users
				SET first_name 		= '".$data['first_name']."',
					last_name 		= '".$data['last_name']."',
					email 			= '".$data['email']."',
					username 		= '".$data['username']."',
					permissions 	= '".$data['permissions']."',
					date_modified 	= NOW()
				WHERE id 			= '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function delete_users($data)
	{
		$sql = "UPDATE users
				SET is_deleted = 1
				WHERE id = '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function update_user_profile($data)
	{
		$sql = "UPDATE users
				SET first_name 		= '".$data['first_name']."',
					last_name 		= '".$data['last_name']."',
					email 			= '".$data['email']."',
					username 		= '".$data['username']."',
					date_modified 	= NOW()
				WHERE id 			= '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function activate_user($data)
	{
		$sql = "UPDATE users
				SET is_deleted 		= 0,
					date_modified 	= NOW()
				WHERE id 			= '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

	function get_pagination($limit = 10, $search = ''){

		$where_query = "";

		if($search !=''){
			$where_query .= " AND (
				    name LIKE '%" . $search . "%'
				    OR
				    description LIKE '%" . $search . "%'
					)";
		}

		 $sql = "SELECT 
					COUNT(*) AS total_count, 
					(
						COUNT(*) % " . $limit . "
					) AS remainder, 
					FLOOR(COUNT(*) / " . $limit . ") AS temp_num_pages
				FROM `users`
				WHERE is_deleted = 0 " . $where_query;


        $result = $this->db->query($sql);
        return $result->result_array();
	}

	function get_users($id = 0, $limit = 0, $offset = 0, $search = '')
	{
		$where_query = '';
		$limit_query = '';

		if($id != 0){
			$where_query .= " AND id = " . $id;
		}

		if($search !=''){
			$where_query .= " AND (
				    CONCAT(u.last_name, ', ', u.first_name) LIKE '%" . $search . "%'
				    OR
				    username LIKE '%" . $search . "%'
				    OR
				    email LIKE '%" . $search . "%'
					)";
		}

		if($limit != 0){
			$limit_query .= " LIMIT " . $offset . "," . $limit;
		}


		$sql = "SELECT 
				  u.id,
				  CONCAT(u.last_name, ', ', u.first_name) full_name,
				  u.first_name,
				  u.last_name,
				  u.email,
				  u.permissions,
				  u.username,
				  u.password,
				  u.is_deleted,
				  DATE_FORMAT(u.last_login, '%M %d, %Y %h:%i %p') AS last_login 
				FROM
				  users u
				WHERE 1 = 1 " . $where_query . "
				ORDER BY (
				    CASE
				      WHEN u.date_modified > u.date_created
				      THEN u.date_modified
				      ELSE u.date_created
				    END
				  ) DESC " . $limit_query;

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	public function check_username($data)
	{
		$sql = "SELECT 
				  u.id,
				  CONCAT(u.last_name, ', ', u.first_name) full_name,
				  u.first_name,
				  u.last_name,
				  u.email,
				  u.permissions,
				  u.username,
				  DATE_FORMAT(u.last_login, '%M %d %Y') AS last_login 
				FROM
				  users u
				WHERE u.is_deleted = 0 
				AND username = '" . $data['username'] . "'
				ORDER BY (
				    CASE
				      WHEN u.date_modified > u.date_created
				      THEN u.date_modified
				      ELSE u.date_created
				    END
				  ) DESC ";

        $result = $this->db->query($sql);
        return $result->result_array();
	}

	public function get_old_password($data)
	{
		$sql = "SELECT 
				  *
				FROM
				  old_password
				WHERE user_id = '" . $data['id'] . "'
				AND old_password = SHA1('" . $data['password'] . "')";

        $result = $this->db->query($sql);
        return $result->row_array();
	}

	function save_old_password($data)
	{
		$sql = "INSERT INTO old_password(
					user_id, 
					old_password)
				VALUES(
				'".$data['id']."', 
				SHA1('" . $data['password'] . "'))";

        $this->db->query($sql);
        return $this->db->insert_id();
	}

	function update_password($data)
	{
		$sql = "UPDATE users
				SET password 		= SHA1('".$data['password']."'),
					date_modified 	= NOW()
				WHERE id 			= '".$data['id']."'";

        $query = $this->db->query($sql);
		return $this->db->affected_rows();
	}

}
?>