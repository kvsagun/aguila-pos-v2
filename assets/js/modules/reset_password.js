$('#_form').submit(function(e){

  e.preventDefault();

  var password = $(".password").val();
  var confirmPassword = $(".confirm-password").val();
  if (password != confirmPassword) {
      var template = "<div class='alert alert-danger' style='margin-top: 10px;'>Passwords do not match.</div>";
      $('#login-message').html(template);
      return false;
  }else{
    
    $.ajax({
        url : base_url + "users/reset_user_password", 
        type: "POST",
        data: $(this).serializeArray(),
        dataType: "JSON",
        success: function(oData){
          if(oData.status) {
            var template = "<div class='alert alert-success' style='margin-top: 10px;'>Password Reset! Redirecting to KVSAGUN POS.</div>";
            $('#login-message').html(template);
            setTimeout(function(){ 
              $(window).attr('location',oData.message);
            }, 3000);
          } else {
            var template = "<div class='alert alert-danger' style='margin-top: 10px;'>" + oData.message + "</div>";
            $('#login-message').html(template);
          }
        }
    });

  }
});  