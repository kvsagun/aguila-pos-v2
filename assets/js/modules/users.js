pagination_url = base_url + "users/get_pagination";

function clear_form(){
  $('#action_type').val('create');
  $('#first_name').val('');
  $('#last_name').val('');
  $('#email').val('');
  $('#username').val('');
  $('#password').val('');
  $('#form-message').html('');
  //permissions
  $('.permission-system_setup').val('0');
  $('.permission-dashboard').val('0');
  $('.permission-category').val('0');
  $('.permission-product').val('0');
  $('.permission-branch').val('0');
  $('.permission-users').val('0');
  $('.permission-promo').val('0');
  $('.permission-pos').val('0');
  $('.permission-pending').val('0');
  $('.permission-sales').val('0');
  $('.permission-inventory').val('0');
  $('.selectpicker').selectpicker('refresh');
}

$('.btn-add').off('click').on('click', function() {
	clear_form();
	$('#createUpdateModal').modal('show');
});

function load_table(offset, limit, search = '') {

  var data = {
      offset 	: offset,
      limit 	: limit,
      search 	: search
  };

  $.ajax({
    type 		     : 'POST',
    url 		     : base_url + "users/load_table_users",
    data 		     : data,
    datatype 	   : 'json',
    returnType 	 : 'json',
    beforeSend	 : function () {
        $('.load-loader').show();
        uiListContainter.empty();
    },
    success 	: function(oData) {
        $('.load-loader').hide();

        var data = oData.data;

        if(offset == 0){
          btnPrevious.attr('disabled', true);                      
          btnPrevious.removeClass('btn-info');                      
          btnPrevious.addClass('btn-outline-info');                      
        }else{
          btnPrevious.attr('disabled', false);    
          btnPrevious.removeClass('btn-outline-info');                      
          btnPrevious.addClass('btn-info');                                 
        }

        if(data.length > (iLimit - 1)) {      
          btnNext.attr('disabled', false);        
          btnNext.removeClass('btn-outline-primary');    
          btnNext.addClass('btn-primary');                 
        } else {
          btnNext.attr('disabled', true);               
          btnNext.removeClass('btn-primary');                      
          btnNext.addClass('btn-outline-primary');                   
        }

        if(data.length > 0) {

            for(var i in data){

                var uiListClone = uiListTemplate.clone();

                uiListClone.addClass('table-list-cloned');
                uiListClone.removeClass('table-list-template');
                uiListClone.find('.count').html(parseInt(i) + parseInt(1));
                uiListClone.find('.full-name').html(data[i].full_name);
                uiListClone.find('.username').html(data[i].username);
                uiListClone.find('.email').html(data[i].email);
                if(data[i].is_deleted == 0){
                  uiListClone.find('.status-true').show();                  
                  uiListClone.find('.status-false').hide();
                }else{
                  uiListClone.find('.status-true').hide();
                  uiListClone.find('.status-false').show();
                  uiListClone.find('.action-btn').hide();
                  uiListClone.find('.user-activate').show();
                }
                uiListClone.find('.last-login').html(data[i].last_login);
                uiListClone.data(data[i]);
                uiListClone.show();
                uiListClone.removeAttr('style');

                if(data[i].id == 1){
                  uiListClone.find('.action').hide();
                }

                uiListContainter.append(uiListClone);                            

            }
  
            var btnEdit         = $('.table-list-cloned').find('.edit');    
            var btnPermission   = $('.table-list-cloned').find('.user-permissions');                
            var btnDelete       = $('.table-list-cloned').find('.delete');                   
            var btnActivate     = $('.table-list-cloned').find('.user-activate');        
                         
            btnEdit.off('click').on('click',function() {
                oParentDetails  = $(this).closest('.table-list-cloned');
                $('#id').val(oParentDetails.data('id'));
                $('#action_type').val('update');
                $('#first_name').val(oParentDetails.data('first_name'));
                $('#last_name').val(oParentDetails.data('last_name'));
                $('#email').val(oParentDetails.data('email'));
                $('#username').val(oParentDetails.data('username'));
                $('#password').prop('required',false);
                $('#password').hide();

                obj             = JSON.parse(oParentDetails.data('permissions'));
                $('.permission-system_setup').val(obj.system_setup[0]);
                $('.permission-dashboard').val(obj.dashboard[0]);
                $('.permission-category').val(obj.category[0]);
                $('.permission-product').val(obj.product[0]);
                $('.permission-branch').val(obj.branch[0]);
                $('.permission-users').val(obj.users[0]);
                $('.permission-promo').val(obj.promo[0]);
                $('.permission-pos').val(obj.pos[0]);
                $('.permission-sales').val(obj.sales[0]);
                $('.permission-pending').val(obj.pending[0]);
                $('.permission-inventory').val(obj.inventory[0]);

                $('.selectpicker').selectpicker('refresh');
                $('#createUpdateModal').modal('show');
            });   
                         
            btnPermission.off('click').on('click',function() {
                oParentDetails  = $(this).closest('.table-list-cloned');

                obj             = JSON.parse(oParentDetails.data('permissions'));
                $('.permission-system_setup').val(obj.system_setup[0]);
                $('.permission-dashboard').val(obj.dashboard[0]);
                $('.permission-category').val(obj.category[0]);
                $('.permission-product').val(obj.product[0]);
                $('.permission-branch').val(obj.branch[0]);
                $('.permission-users').val(obj.users[0]);
                $('.permission-promo').val(obj.promo[0]);
                $('.permission-pos').val(obj.pos[0]);
                $('.permission-sales').val(obj.sales[0]);
                $('.permission-pending').val(obj.pending[0]);
                $('.permission-inventory').val(obj.inventory[0]);

                $('.selectpicker').selectpicker('refresh');

                $('#viewUserPermissionModal').modal('show');
            });      

            btnDelete.off('click').on('click',function() {
                oParentDetails 	= $(this).closest('.table-list-cloned');

                Swal.fire({
                  title: 'Are you sure to deactivate, ' + oParentDetails.data('username') + '?',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonColor: '#3085d6',
                  cancelButtonColor: '#d33',
                  confirmButtonText: 'Yes!'
                }).then((result) => {
                  if (result.value) {

                    // delete
                    var data = {
                      id      : oParentDetails.data('id'),
                      action_type   : 'delete'
                    };

                    $.ajax({
                      url     : base_url + "users/save_users", 
                      type    : "post",
                      data    : data,
                      dataType  : "JSON",
                      success   : function(data){
                        $('#confirmModal').modal('hide');
                        load_table(iOffset, iLimit);
                        set_table_pagination(pagination_url);
                        Toast.fire({
                          type: 'success',
                          title: 'Successfully deleted'
                        });
                      }
                    });
                }
              })

                // $('.confirm-message').html('Are you sure you want to deactivate, ' + oParentDetails.data('username') + '?');
                // $('#confirmModal').modal('show');
            });

            $('.user-change-password').off('click').on('click', async function(e) {
              e.preventDefault();

              const {value: formValues} = await Swal.fire({
                title: 'Change Password',
                html:
                  '<input id="new_password" type="password" placeholder="New Password" class="swal2-input">' +
                  '<input id="confirm_password" type="password" placeholder="Confirm Password" class="swal2-input">',
                focusConfirm: false,
                preConfirm: () => {
                  return [
                    document.getElementById('new_password').value,
                    document.getElementById('confirm_password').value
                  ]
                }
              });

              if (formValues) {
                Swal.fire(JSON.stringify(formValues))
              }

            });

            btnActivate.off('click').on('click', async function(e) {
              oParentDetails  = $(this).closest('.table-list-cloned');

              Swal.fire({
                title: 'Are you sure to activate, ' + oParentDetails.data('username') + '?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes!'
              }).then((result) => {
                if (result.value) {
                  $.ajax({
                      url : base_url + "users/activate_user", 
                      type: "POST",
                      data: {
                        id : oParentDetails.data('id'),
                      },
                      dataType: "JSON",
                      beforeSend   : function () {
                        swal.fire({
                            title: 'Please Wait..!',
                            text: 'Is working..',
                            allowOutsideClick: false,
                            allowEscapeKey: false,
                            allowEnterKey: false,
                            onOpen: () => {
                                swal.showLoading()
                            }
                        })
                      },
                      success: function(oData){
                        swal.hideLoading();
                        Swal.fire(
                          'Activated!',
                          'The account successfully activated.',
                          'success'
                        );
                        Toast.fire({
                          type: 'success',
                          title: 'Successfully activated'
                        });
                        load_table(iOffset, iLimit);
                        set_table_pagination(pagination_url);
                      }
                  });
                }
              })
            });

        } else if(data.length 	== 0) {

            uiListContainter.append('<div class="text-center">No Records Found.</div>');
            
        }

    }
  });
}

load_table(iOffset, iLimit);

set_table_pagination(pagination_url);


/**********************************************/
/**********************************************/
/*            ADD EDIT DELETE DATA            */
/**********************************************/
/**********************************************/  

$('#_form').submit(function(e){
	var id = $('#id').val();

	e.preventDefault();

	$.ajax({
    url  		: base_url + "users/save_users", 
    type 		: "post",
    data 		: $(this).serializeArray(),
    dataType 	: "JSON",
    success: function(data){
      if(data.message){
        var template = "<div class='alert alert-danger' style='margin-top: 10px;'>" + data.message + "</div>";
        $('#form-message').html(template);
        $('#form-message').animate({ scrollTop: 0 }, 'slow');
        Toast.fire({
          type: 'warning',
          title: data.message
        });
      }else{
        $('#createUpdateModal').modal('hide');
        load_table(iOffset, iLimit);
        set_table_pagination(pagination_url);
        clear_form();
        $('#form-message').html('');
        Toast.fire({
          type: 'success',
          title: 'Successfully save'
        });
      }
    }
  });
});  

btnConfirmDelete.off('click').on('click', function() {
  // delete
  var data = {
    id 			: oParentDetails.data('id'),
    action_type 	: 'delete'
  };

  $.ajax({
    url 		: base_url + "users/save_users", 
    type    : "post",
    data 		: data,
    dataType 	: "JSON",
    success 	: function(data){
      $('#confirmModal').modal('hide');
      load_table(iOffset, iLimit);
      set_table_pagination(pagination_url);
      Toast.fire({
        type: 'success',
        title: 'Successfully deleted'
      });
    }
  });
});