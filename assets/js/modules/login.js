$('#_form').submit(function(e){
  e.preventDefault();
  $.ajax({
      url : base_url + "login/authorize_user", 
      type: "POST",
      data: $(this).serializeArray(),
      dataType: "JSON",
      success: function(oData){
        if(oData.status) {
          var template = "<div class='alert alert-success' style='margin-top: 10px;'>Redirecting</div>";
          $('#login-message').html(template);
          setTimeout(function(){ 
            $(window).attr('location',oData.message);
          }, 1000);
        } else {
          var template = "<div class='alert alert-danger' style='margin-top: 10px;'>" + oData.message + "</div>";
          $('#login-message').html(template);
        }
      }
    });
});  

$('.forgot-password').off('click').on('click', async function(e) {
  e.preventDefault();

  const {value: email} = await Swal.fire({
    title: 'Input email address',
    input: 'email',
    inputPlaceholder: 'Enter your email address'
  });

  if (email) {
    $.ajax({
        url : base_url + "login/send_forgot_password_email", 
        type: "POST",
        data: {
          email : email,
        },
        dataType: "JSON",
        beforeSend   : function () {
          swal.fire({
              title: 'Please Wait..!',
              text: 'Is working..',
              allowOutsideClick: false,
              allowEscapeKey: false,
              allowEnterKey: false,
              onOpen: () => {
                  swal.showLoading()
              }
          })
        },
        success: function(oData){
          swal.hideLoading();
          if(oData.status) {
            Swal.fire({
              type: 'success',
              text: oData.message
            });
          } else {
            Swal.fire({
              type: 'error',
              text: oData.message
            });
          }
        }
      });
  }
});

