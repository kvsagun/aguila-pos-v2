load_profile();

function load_profile(){
	$.ajax({
    type 		     : 'POST',
    url 		     : base_url + "users/get_users_by_id",
    data 		     : {
    	id	: user_id
    },
    datatype 	   	 : 'json',
    returnType 	 	 : 'json',
    beforeSend	 : function () {

    },
    success 	: function(oData) {

        var data = oData.data;

        if(data.length > 0) {

            for(var i in data){

            	$('.username').html(data[i].username);
            	$('.email').html(data[i].email);
            	$('.first-name').html(data[i].first_name);
            	$('.last-name').html(data[i].last_name);
            	
            	$('.user-first-name').html(data[i].first_name);

                $('#users_data').data(data[i]);                         

            }

        } else if(data.length 	== 0) {

        	// $('#users_data').data('');
            
        }

    }
  });
}

$('.edit').off('click').on('click',function() {
    oParentDetails 	= $('#users_data');
	$('#id').val(oParentDetails.data('id'));  
	$('#username').val(oParentDetails.data('username'));  
	$('#first_name').val(oParentDetails.data('first_name'));  
	$('#last_name').val(oParentDetails.data('last_name'));  
	$('#email').val(oParentDetails.data('email'));  
    $('#createUpdateModal').modal('show'); 
});    

function clear_form(){
  $('#action_type').val('create');
  $('#first_name').val('');
  $('#last_name').val('');
  $('#email').val('');
  $('#username').val('');
  $('#form-message').html('');
}

$('#_form').submit(function(e){
	var id = $('#id').val();

	e.preventDefault();

	$.ajax({
	    url  		: base_url + "users/update_user_profile", 
	    type 		: "post",
	    data 		: $(this).serializeArray(),
	    dataType 	: "JSON",
	    success: function(data){
	      $('#createUpdateModal').modal('hide');
	      load_profile();
	      clear_form();
        Toast.fire({
          type: 'success',
          title: 'Successfully save'
        });
	    }
  });
}); 