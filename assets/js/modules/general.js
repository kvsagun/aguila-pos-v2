load_company();
load_senior();
load_vat();

function load_company(){
	$.ajax({
    type 		     : 'POST',
    url 		     : base_url + "general/get_company",
    data 		     : {
    	action	: 'get'
    },
    datatype 	   	 : 'json',
    returnType 	 	 : 'json',
    beforeSend	 : function () {

    },
    success 	: function(oData) {

        var data = oData.data;

        if(data.length > 0) {

            for(var i in data){

            	$('.company-logo').attr('src', base_url + 'assets/img/' + data[i].logo);
            	$('.company-name').html(data[i].name);

                $('#company').data(data[i]);                         

            }

        } else if(data.length 	== 0) {

			$('.company-logo').attr('src', base_url + 'assets/img/kvsagun-pos-logo.png');
        	$('.company-name').html('KVSAGUN POS');
            
        }

    }
  });
}

function load_senior(){
	$.ajax({
    type 		     : 'POST',
    url 		     : base_url + "general/get_senior",
    data 		     : {
    	action	: 'get'
    },
    datatype 	   	 : 'json',
    returnType 	 	 : 'json',
    beforeSend	 : function () {

    },
    success 	: function(oData) {

        var data = oData.data;

        if(data.length > 0) {

            for(var i in data){

            	$('.senior-discount').html(data[i].discount + "%");

                $('#senior').data(data[i]);                         

            }

        } else if(data.length 	== 0) {

        	$('.senior-discount').html('12%');
            
        }

    }
  });
}

function load_vat(){
	$.ajax({
    type 		     : 'POST',
    url 		     : base_url + "general/get_vat",
    data 		     : {
    	action	: 'get'
    },
    datatype 	   	 : 'json',
    returnType 	 	 : 'json',
    beforeSend	 : function () {

    },
    success 	: function(oData) {

        var data = oData.data;

        if(data.length > 0) {

            for(var i in data){

            	$('.vat').html(data[i].tax + "%");

                $('#vat').data(data[i]);                         

            }

        } else if(data.length 	== 0) {

        	$('.vat').html('12%');
            
        }

    }
  });
}                

$('.edit-company').off('click').on('click',function() {
    oParentDetails 	= $('#company');
	$('#company_id').val(oParentDetails.data('id'));  
	$('#company_name').val(oParentDetails.data('name'));  
    $('#updateCompanyModal').modal('show'); 
}); 

function clear_company_form(){
  $('#company_id').val('');
  $('#company_name').val('');
  $('#company_logo').val('');
}

$('#_company_form').submit(function(e){
    var id = $('#company_id').val();

    e.preventDefault();

    $.ajax({
        url         : base_url + "General/update_company", 
        type        : "post",
        data        : new FormData(this),
        contentType : false,
        processData : false,
        cache       : false,
        async       : false,     
        success: function(data){
          $('#updateCompanyModal').modal('hide');
          load_company();
          clear_company_form();
            Toast.fire({
              type: 'success',
              title: 'Successfully save'
            });
        }
  });
});            

$('.edit-senior').off('click').on('click',function() {
    oParentDetails 	= $('#senior');
	$('#senior_id').val(oParentDetails.data('id'));  
	$('#senior_discount').val(oParentDetails.data('discount'));  
    $('#updateSeniorModal').modal('show'); 
});    

function clear_senior_form(){
  $('#senior_id').val('');
  $('#senior_discount').val('');
} 

$('#_senior_form').submit(function(e){
    var id = $('#senior_id').val();

    e.preventDefault();

    $.ajax({
        url         : base_url + "general/update_senior", 
        type        : "post",
        data        : $(this).serializeArray(),
        dataType    : "JSON",
        success: function(data){
          $('#updateSeniorModal').modal('hide');
          load_senior();
          clear_senior_form();
          Toast.fire({
            type: 'success',
            title: 'Successfully save'
          });
        }
  });
});            

$('.edit-vat').off('click').on('click',function() {
    oParentDetails 	= $('#vat');
	$('#vat_id').val(oParentDetails.data('id'));  
	$('#tax_percent').val(oParentDetails.data('tax'));  
    $('#updateVatModal').modal('show'); 
});     

function clear_vat_form(){
  $('#vat_id').val('');
  $('#tax_percent').val('');
} 

$('#_vat_form').submit(function(e){
    var id = $('#vat_id').val();

    e.preventDefault();

    $.ajax({
        url         : base_url + "general/update_vat", 
        type        : "post",
        data        : $(this).serializeArray(),
        dataType    : "JSON",
        success: function(data){
          $('#updateVatModal').modal('hide');
          load_vat();
          clear_vat_form();
          Toast.fire({
            type: 'success',
            title: 'Successfully save'
          });
        }
  });
});            
  